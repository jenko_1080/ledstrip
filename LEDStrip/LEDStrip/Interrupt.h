#pragma once
//#include "common.h"
#include <stdint.h>
//#include <InterruptHandler.h>	// TODO: Remove this line, use an object factory to perform interrupt registration

class Interrupt
{
public:
	Interrupt();
	virtual ~Interrupt() = 0;
	virtual void ISR() = 0;

protected:
	virtual void InitInterrupt(uint32_t HardwareAddress, uint8_t uiPriority, uint8_t uiSubPriority) = 0;
	virtual void DeinitInterrupt(uint32_t HardwareAddress) = 0;
};