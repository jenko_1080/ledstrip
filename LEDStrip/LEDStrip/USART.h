#pragma once
#include <stdint.h>
#include <Interrupt.h>
#include <InterruptHandler.h>
#include <stm32f4xx_gpio.h>
#include <stm32f4xx_usart.h>
#include <stm32f4xx_rcc.h>
#include <misc.h>

// Abstract base class for anything that uses UART hardware
class USART : public Interrupt
{
public:
	USART();
	virtual ~USART();

	virtual void initialise() = 0;
protected:
	// Kinda important functions for a UART
	virtual void Rx(uint8_t *) = 0;
	virtual void Tx(uint8_t *) = 0;

	// Hardware initialisation
	uint32_t InitialiseHardware(uint32_t HardwareAddress, uint32_t uiBaudRate, uint16_t uiWordLength, uint16_t uiParity, uint16_t uiStopBits);
	uint32_t DeinitialiseHardware(uint32_t HardwareAddress);
	// Interrupt initialisation
	virtual void InitInterrupt(uint32_t HardwareAddress, uint8_t uiPriority, uint8_t uiSubPriority) override;
	virtual void InitInterrupt(uint32_t HardwareAddress, uint8_t uiPriority, uint8_t uiSubPriority, FunctionalState RXInt, FunctionalState TXInt);
	virtual void DeinitInterrupt(uint32_t HardwareAddress) override;

	// Supporting functions
	void peripheralClock(uint32_t HardwareAddress, FunctionalState newState);
	IRQn_Type getUSARTIRQn(uint32_t HardwareAddress);
	void configurePins(uint32_t HardwareAddress, uint8_t pinGroup);
};

