#include "USART.h"


USART::USART()
{
	
}

USART::~USART()
{
}

uint32_t USART::InitialiseHardware(uint32_t HardwareAddress, uint32_t uiBaudRate, uint16_t uiWordLength, uint16_t uiParity, uint16_t uiStopBits)
{
	// Enable hardware clock
	peripheralClock(HardwareAddress, ENABLE);

	// USART Setup Information
	USART_InitTypeDef USART_InitStruct;
	USART_InitStruct.USART_BaudRate = uiBaudRate;
	USART_InitStruct.USART_WordLength = uiWordLength;
	USART_InitStruct.USART_Parity = uiParity;
	USART_InitStruct.USART_StopBits = uiStopBits;
	USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

	// Send data to USART HW
	USART_Init((USART_TypeDef *)HardwareAddress, &USART_InitStruct);
	// Enable USART Module
	USART_Cmd((USART_TypeDef *)HardwareAddress, ENABLE);
}

uint32_t USART::DeinitialiseHardware(uint32_t HardwareAddress)
{
	// Deinit Hardware
	USART_DeInit((USART_TypeDef *)HardwareAddress);
	// Disable USART Module
	USART_Cmd((USART_TypeDef *)HardwareAddress, DISABLE);

	// Disable hardware clock
	peripheralClock(HardwareAddress, DISABLE);
}

void USART::InitInterrupt(uint32_t HardwareAddress, uint8_t uiPriority, uint8_t uiSubPriority)
{
	// Set up interrupt controller for usart
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = getUSARTIRQn(HardwareAddress);// Set IRQ channel
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;					// Enable Interrupts
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = uiPriority;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = uiSubPriority;
	NVIC_Init(&NVIC_InitStruct);

	// Clear any current interrupt flags
	USART_ClearITPendingBit((USART_TypeDef *)HardwareAddress, USART_IT_RXNE);	// Clear RX interrupt bit
	USART_ClearITPendingBit((USART_TypeDef *)HardwareAddress, USART_IT_TXE);	// Clear TX interrupt bit

	USART_ITConfig((USART_TypeDef *)HardwareAddress, USART_IT_RXNE, ENABLE);
	USART_ITConfig((USART_TypeDef *)HardwareAddress, USART_IT_TXE, DISABLE);
}

void USART::InitInterrupt(uint32_t HardwareAddress, uint8_t uiPriority, uint8_t uiSubPriority, FunctionalState RXInt, FunctionalState TXInt)
{
	// Set up interrupt controller for usart
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = getUSARTIRQn(HardwareAddress);// Set IRQ channel
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;					// Enable Interrupts
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = uiPriority;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = uiSubPriority;
	NVIC_Init(&NVIC_InitStruct);

	// Clear any current interrupt flags
	USART_ClearITPendingBit((USART_TypeDef *)HardwareAddress, USART_IT_RXNE);	// Clear RX interrupt bit
	USART_ClearITPendingBit((USART_TypeDef *)HardwareAddress, USART_IT_TXE);	// Clear TX interrupt bit

	USART_ITConfig((USART_TypeDef *)HardwareAddress, USART_IT_RXNE, RXInt);
	USART_ITConfig((USART_TypeDef *)HardwareAddress, USART_IT_TXE, TXInt);
}

void USART::DeinitInterrupt(uint32_t HardwareAddress)
{
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = getUSARTIRQn(HardwareAddress);// Set IRQ channel
	NVIC_InitStruct.NVIC_IRQChannelCmd = DISABLE;					// Enable Interrupts
	NVIC_Init(&NVIC_InitStruct);
}

void USART::peripheralClock(uint32_t HardwareAddress, FunctionalState newState)
{
	switch (HardwareAddress)
	{
	case USART1_BASE:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, newState);
		break;
	case USART2_BASE:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, newState);
		break;
	case USART3_BASE:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, newState);
		break;
	case UART4_BASE:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART4, newState);
		break;
	case UART5_BASE:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, newState);
		break;
	case USART6_BASE:
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART6, newState);
		break;
	case UART7_BASE:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART7, newState);
		break;
	case UART8_BASE:
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART8, newState);
		break;
	}
}

IRQn_Type USART::getUSARTIRQn(uint32_t HardwareAddress)
{
	IRQn_Type IRQn;

	switch (HardwareAddress)
	{
	case USART1_BASE:
		IRQn = USART1_IRQn;
		break;
	case USART2_BASE:
		IRQn = USART2_IRQn;
		break;
	case USART3_BASE:
		IRQn = USART3_IRQn;
		break;
	case UART4_BASE:
		IRQn = UART4_IRQn;
		break;
	case UART5_BASE:
		IRQn = UART5_IRQn;
		break;
	case USART6_BASE:
		IRQn = USART6_IRQn;
		break;
	}

	return IRQn;
}

void USART::configurePins(uint32_t HardwareAddress, uint8_t pinGroup)
{
	// Set up common elements
	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_25MHz;

	switch (HardwareAddress)
	{
	case USART1_BASE:
		// PA 9 10
		// PB 6 7
		if (pinGroup == 1)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
			GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1);
			GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
			GPIO_Init(GPIOA, &GPIO_InitStruct);
		}
		else if (pinGroup == 2)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
			GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_USART1);
			GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_USART1);
			GPIO_Init(GPIOB, &GPIO_InitStruct);
		}
		break;
	case USART2_BASE:
		// PA 2 3
		// PB 5 6
		if (pinGroup == 1)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
			GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
			GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);
			GPIO_Init(GPIOA, &GPIO_InitStruct);
		}
		else if (pinGroup == 2)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
			GPIO_PinAFConfig(GPIOD, GPIO_PinSource5, GPIO_AF_USART2);
			GPIO_PinAFConfig(GPIOD, GPIO_PinSource6, GPIO_AF_USART2);
			GPIO_Init(GPIOD, &GPIO_InitStruct);
		}
		break;
	case USART3_BASE:
		// PB 10 11
		// PC 10 11
		// PD 8 9
		if (pinGroup == 1)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
			GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3);
			GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3);
			GPIO_Init(GPIOB, &GPIO_InitStruct);
		}
		else if (pinGroup == 2)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_USART3);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_USART3);
			GPIO_Init(GPIOC, &GPIO_InitStruct);
		}
		else if (pinGroup == 3)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
			GPIO_PinAFConfig(GPIOD, GPIO_PinSource8, GPIO_AF_USART3);
			GPIO_PinAFConfig(GPIOD, GPIO_PinSource9, GPIO_AF_USART3);
			GPIO_Init(GPIOD, &GPIO_InitStruct);
		}
		break;
	case UART4_BASE:
		// PA 0 1
		// PC 10 11
		if (pinGroup == 1)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0| GPIO_Pin_1;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
			GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_UART4);
			GPIO_PinAFConfig(GPIOA, GPIO_PinSource1, GPIO_AF_UART4);
			GPIO_Init(GPIOA, &GPIO_InitStruct);
		}
		else if (pinGroup == 2)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10 | GPIO_Pin_11;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_UART4);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_UART4);
			GPIO_Init(GPIOC, &GPIO_InitStruct);
		}
		break;
	case UART5_BASE:
		//PC x 12
		//PD 2 x
		if (pinGroup == 1)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource12, GPIO_AF_UART5);
			GPIO_Init(GPIOC, &GPIO_InitStruct);

			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
			GPIO_PinAFConfig(GPIOD, GPIO_PinSource2, GPIO_AF_UART5);
			GPIO_Init(GPIOD, &GPIO_InitStruct);
		}
		break;
	case USART6_BASE:
		// PC 6 7
		// PG 14 9
		if (pinGroup == 1)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_USART6);
			GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_USART6);
			GPIO_Init(GPIOC, &GPIO_InitStruct);
		}
		else if (pinGroup == 2)
		{
			GPIO_InitStruct.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_9;
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
			GPIO_PinAFConfig(GPIOG, GPIO_PinSource14, GPIO_AF_USART6);
			GPIO_PinAFConfig(GPIOG, GPIO_PinSource9, GPIO_AF_USART6);
			GPIO_Init(GPIOG, &GPIO_InitStruct);
		}
		break;
	case UART7_BASE:
		// Not valid for STM32F407VG
		break;
	case UART8_BASE:
		// Not valid for STM32F407VG
		break;
	}
}
