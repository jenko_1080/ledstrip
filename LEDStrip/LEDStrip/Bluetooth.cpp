#include "Bluetooth.h"



Bluetooth::Bluetooth(uint32_t HardwareAddress, uint8_t pinGroup)
{
	this->HardwareAddress = HardwareAddress;
	this->pinGroup = pinGroup;

	initialise();
}

Bluetooth::~Bluetooth()
{
}

void Bluetooth::ISR()
{
	// TX Interrupt
	if (USART_GetITStatus((USART_TypeDef *)HardwareAddress, USART_IT_TXE) != RESET)
	{
		count++;
	}
	// RX Interrupt
	if (USART_GetITStatus((USART_TypeDef *)HardwareAddress, USART_IT_RXNE) != RESET)
	{
		uint8_t cRX = USART_ReceiveData((USART_TypeDef *)HardwareAddress);
		USART_SendData((USART_TypeDef *)HardwareAddress, cRX);
	}
}

void Bluetooth::initialise()
{
	configurePins(HardwareAddress, pinGroup);
	InitialiseHardware(HardwareAddress, 9600, USART_WordLength_8b, USART_Parity_No, USART_StopBits_1);
	InitInterrupt(HardwareAddress, 0, 0);
}

void Bluetooth::Rx(uint8_t *)
{
	// Static vars to track recieve state
	static uint8_t rxState = 0;
	// Buffer to hold current recieve string
	static uint8_t rxString[256];
	
	// Recieve data from BT Hardware
	// - ALL strings start with $, are separated with commas, terminate with *[0-9][0-9]\r\n

}

void Bluetooth::Tx(uint8_t *)
{
	return;
}
