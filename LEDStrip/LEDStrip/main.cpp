#include <stm32f4xx_usart.h>
#include "DeviceFactory.h"
#include "InterruptHandler.h"
#include "Bluetooth.h"

extern "C" {	void(*callISR_c)(uint32_t); }
void callISR(uint32_t HardwareAddress);

int main()
{
	// Register Function Pointer for Interrupts
	callISR_c = callISR;

	// Create a BT device
	USART * BT = DeviceFactory::createUSARTDevice(DeviceFactory::Bluetooth_t, USART1_BASE, 2, true);

	//InterruptHandler::CallISR(USART1_BASE);
	for (;;)
	{
		
	}
}

// TODO:: Create singleton class "Devices" -> creates the class, registers the class using InterruptHandler


////////////////
// Interrupts //
////////////////
// C++ function to call Appropriate ISR
void callISR(uint32_t HardwareAddress)
{
	InterruptHandler::CallISR(HardwareAddress);
}

// No need to touch anything below here (unless its missing)
extern "C" void USART1_IRQHandler()
{
	callISR(USART1_BASE);
}
extern "C" void USART2_IRQHandler()
{
	callISR(USART2_BASE);
}
extern "C" void USART3_IRQHandler()
{
	callISR(USART3_BASE);
}
extern "C" void UART4_IRQHandler()
{
	callISR(UART4_BASE);
}
extern "C" void UART5_IRQHandler()
{
	callISR(UART5_BASE);
}
extern "C" void USART6_IRQHandler()
{
	callISR(USART6_BASE);
}