#pragma once
#include "InterruptHandler.h"
#include "USART.h"
#include "Bluetooth.h"

class DeviceFactory
{
public:
	enum DeviceTypes {
		Bluetooth_t,
		WiFi_t
	};

	static USART * createUSARTDevice(DeviceTypes type, uint32_t HardwareAddress, uint8_t pinGroup, bool registerISR);
};

