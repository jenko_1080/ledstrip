#include "DeviceFactory.h"

USART * DeviceFactory::createUSARTDevice(DeviceTypes type, uint32_t HardwareAddress, uint8_t pinGroup, bool registerISR)
{
	USART * newDevice = nullptr;

	// Create new device
	switch (type)
	{
	case Bluetooth_t:
		newDevice = new Bluetooth(HardwareAddress, pinGroup);
	}

	// Register ISR
	if (registerISR)
	{
		InterruptHandler::RegisterISR(HardwareAddress, newDevice);
	}

	return newDevice;
}
