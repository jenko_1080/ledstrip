#pragma once

#include <stdint.h>		// not worried about namespace, *probably* won't be an issue
//#include <functional>	// to store member functions using std::mem_fn (Using polymorphism instead, seems more logical)
#include <map>			// Container to store all HardwareAddress->Object(->ISR) data
#include <Interrupt.h>	// Objects that can be stored will implement Interrupt class

class InterruptHandler
{
public:
	// Call the appropriate ISR from the multimap
	static void CallISR(uint32_t);
	// Add a new ISR to the multimap
	static void RegisterISR(uint32_t, Interrupt*);
	// Remove an ISR from the multimap
	static void DeregisterISR(uint32_t, Interrupt*);
	// Remove all ISRs related to a piece of hardware
	static void RemoveISRRange(uint32_t);
//private:
	// Declare a static data structure to hold all ISRs <HWAddress, Object>
	static std::multimap<uint32_t, Interrupt*> ISRMap;

};

