#include "InterruptHandler.h"

// Define ISRMap !
std::multimap<uint32_t, Interrupt*> InterruptHandler::ISRMap;

void InterruptHandler::CallISR(uint32_t HardwareAddress)
{
	// Create var to hold iterators for beginning / end of multimap range
	//std::pair <std::multimap<uint32_t, Interrupt*>::iterator, std::multimap<uint32_t, Interrupt*>::iterator> range;

	// Get range of ISRs to execute
	auto range = ISRMap.equal_range(HardwareAddress);

	// Run range of Interrupt Service Routines
	for (auto it = range.first; it != range.second; ++it)
	{
		//  (Object)->ISR()
		(it->second)->ISR();
	}
}

// Expensive way of doing things, but should only be used once per ISR
void InterruptHandler::RegisterISR(uint32_t HardwareAddress, Interrupt* objectPtr)
{
	// Function vars
	bool exists = false;

	// Create var to hold iterators for beginning / end of multimap range
	//std::pair <std::multimap<uint32_t, Interrupt*>::iterator, std::multimap<uint32_t, Interrupt*>::iterator> range;

	// Get range of ISRs that already exist
	auto range = ISRMap.equal_range(HardwareAddress);

	// Ensure ISR isn't already in here
	for (auto it = range.first; it != range.second; ++it)
	{
		if (it->second == objectPtr)
		{
			exists = true;
			break;
		}
	}

	// Add the ISR if it doesn't exist
	if (!exists)
	{
		ISRMap.insert(std::pair<uint32_t, Interrupt*>(HardwareAddress, objectPtr));
	}
}

// Expensive way of doing things, but should only be used once per ISR
void InterruptHandler::DeregisterISR(uint32_t HardwareAddress, Interrupt* objectPtr)
{
	// Function vars
	//std::multimap<uint32_t, Interrupt*>::iterator location = ISRMap.end();
	auto location = ISRMap.end();

	// Create var to hold iterators for beginning / end of multimap range
	//std::pair <std::multimap<uint32_t, Interrupt*>::iterator, std::multimap<uint32_t, Interrupt*>::iterator> range;

	// Get range of ISRs that exist exist
	auto range = ISRMap.equal_range(HardwareAddress);

	// Find ISR to remove
	for (auto it = range.first; it != range.second; ++it)
	{
		if (it->second == objectPtr)
		{
			location = it;
			break;
		}
	}

	// Remove the ISR if it was found
	if (location != ISRMap.end())
	{
		ISRMap.erase(location);
	}
}

void InterruptHandler::RemoveISRRange(uint32_t HardwareAddress)
{
	// Create var to hold iterators for beginning / end of multimap range
	//std::pair <std::multimap<uint32_t, Interrupt*>::iterator, std::multimap<uint32_t, Interrupt*>::iterator> range;

	// Get range of ISRs that exist exist
	auto range = ISRMap.equal_range(HardwareAddress);

	// Erase all ISRs
	ISRMap.erase(range.first, range.second);
}
