#pragma once
#include "USART.h"

class Bluetooth :
	public USART
{
public:
	Bluetooth(uint32_t HardwareAddress, uint8_t pinGroup);
	~Bluetooth();
	virtual void ISR() override;
	virtual void initialise() override;
private:
	virtual void Rx(uint8_t *) override;
	virtual void Tx(uint8_t *) override;

	uint32_t count = 0;
	uint32_t HardwareAddress;
	uint8_t pinGroup;
};

